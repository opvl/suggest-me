<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Suggestion;

class SuggestionController extends Controller
{
    public function index(Request $request)
    {
        $take = $request->take ?? 10;
        $suggestions = Suggestion::orderBy('created_at', 'desc')->paginate($take);
        return view('welcome', ['suggestions' => $suggestions]);
    }

    public function getSuggestion($id)
    {
        $suggestion = Suggestion::find($id);
        return view('home.suggestion', ['suggestion' => $suggestion]);
    }

    public function orderByLike(){
        $suggestions = Suggestion::withCount('likes')->orderByDesc('likes_count')->get()->take(3);
        return view('home.index', ['suggestions' => $suggestions]);
    }
}
