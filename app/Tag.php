<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function suggestions()
    {
        return $this->belongsToMany('App\Suggestion')
            ->withTimestamps();
    }
}
