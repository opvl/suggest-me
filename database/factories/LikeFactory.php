<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;
use App\Like;
use PhpParser\Node\Expr\Cast\Int_;

$factory->define(Like::class, function (Faker $faker) {
    return [
        'suggestion_id' => random_int(0, 49)
    ];
});
