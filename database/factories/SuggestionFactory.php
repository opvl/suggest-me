<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;
use App\Suggestion;

$factory->define(Suggestion::class, function (Faker $faker) {
    return [
        'summary' => $faker->word,
        'details' => $faker->sentence(9)
    ];
});
