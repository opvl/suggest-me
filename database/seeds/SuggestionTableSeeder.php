<?php

use Illuminate\Database\Seeder;

class SuggestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Suggestion::class, 50)->create()->each(function ($suggestion) {
            $suggestion->tags()->save(factory(App\Tag::class)->make());
        });
    }
}
