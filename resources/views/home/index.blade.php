@extends('layouts.master')

@section('content')
<style>
    html,
    body {
        background-color: #fff;
        color: #636b6f;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
        height: 100vh;
        margin: 0;
    }

    .full-height {
        height: 100vh;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

    .top-right {
        position: absolute;
        right: 10px;
        top: 18px;
    }

    .content {
        text-align: center;
    }

    .title {
        font-size: 84px;
    }

    .subtitle {
        font-size: 18px;
    }

    .links>a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 13px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
    }

    .m-b-md {
        margin-bottom: 15px;
    }

    .suggestion-feed {
        width: 90%;
        margin: auto;
        font-size: 12px;
    }

    .form-group{
        max-height: 50px;
    }

</style>

<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
    <div class="top-right links">
        @auth
        <a href="{{ url('/home') }}">Home</a>
        @else
        <a href="{{ route('login') }}">Login</a>

        @if (Route::has('register'))
        <a href="{{ route('register') }}">Register</a>
        @endif
        @endauth
    </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            suggest.me
            <div class="form-group">
                    <input type="text" class="form-control text-center" name="summary" id="summary"
                        aria-describedby="summaryHelp" placeholder="the online suggestion box. . ."><br>
                </div>
        </div>


        <div class="card suggestion-feed">
            <ul class="list-group list-group-flush">
                @foreach ($suggestions as $suggestion)
                <a class="list-group-item" href="{{ route('home.suggestion', ['id' => $suggestion->id]) }}">
                    <h4 class="card-title">{{$suggestion->summary}}</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text">
                                @foreach ($suggestion->tags as $tag)
                                - {{ $tag->name }} -
                                @endforeach
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p class="card-text">{{count($suggestion->likes)}}</p>
                        </div>
                    </div>
                </a>
                @endforeach
            </ul>
        </div>
    </div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>
@endsection
