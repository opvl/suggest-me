<style>
    .footer {
        width: 100%;
        position: fixed;
        bottom: 0;
        font-size: 12px;
    }

    .flex-row {
        display: flex;
        flex-direction: row;
        max-width: 300px;
        margin: auto;
        text-decoration: none;
        list-style-type: none;
        align-content: center;
        justify-content: space-around;
        height: 40px;
    }

    ul {
        padding: 0;
    }

    .flex-row li {
        color: #333;
        padding: 5px;
        margin: auto;
    }

    a{
        color: inherit;
        transition: 0.2s ease-in-out color;
        font-weight: 800;

    }

    a:hover{
        text-decoration: none;
        color: #ea3ee2;
    }

</style>

<div class="footer">
    <ul class="flex-row">
        <li><a href="#" class="active">Active item</a></li>
        <li><a href="#">Item</a></li>
        <li><a href="#">Disabled item</a></li>
    </ul>
</div>
